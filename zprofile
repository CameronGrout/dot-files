# Copy (not link) this file to $HOME and configure

# Indicate that the profile file was sourced, so that later
# scripts can detect possible failures. Specifically added
# for issues with alacritty not using a login shell
export ZPROFILE_SOURCED=1

# Path to the root of the texlive install directory
#export TEXLIVE_BASE=

# Path to the root of the anaconda install directory 
#export ANACONDA_BASE=

# Path to the root of the dot-files repository
#export DOTFILES_BASE=

# XDG config dir
export XDG_CONFIG_HOME=$HOME/.config
