" Load vim-plug {{{
if empty(glob('~/.local/share/nvim/site/autoload/plug.vim'))
        silent !curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
        autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif
" }}}

let maplocalleader=","

" Behaviors {{{
filetype plugin on
set clipboard=unnamedplus " Clipboard is default register
set backspace=indent,eol,start
set notimeout " Chorded keystrokes have no timeout
set expandtab
set lazyredraw
set ignorecase " Lowercase patterns ignore case
set smartcase " Uppercase in patterns overrides ignorecase
set splitright
set splitbelow
set incsearch
set matchpairs+=<:> " Match tags as brackets
set foldmarker={{{,}}}
set foldmethod=marker
set inccommand=nosplit
set wildmenu
set wildmode=list:longest,full
set omnifunc=syntaxcomplete#Complete
" }}}

" Visual Features {{{
set cursorline
set modeline
set ruler
set number " If set with relativenumber, current line is actual number instead of 0
set relativenumber
set showmatch
set list " Show whitespace characters
set listchars=tab:»·,eol:¬,nbsp:·,precedes:…,extends:…,trail:·
" }}}

" Disable irritating files
set nowritebackup
set noswapfile

" Extension filetype associations {{{
autocmd BufReadPost *.tex set syntax=tex
" }}}

" Plugin loading {{{
call plug#begin()

" Appearance
Plug 'OISumeko/vim-wombat-scheme'

" Functionality
Plug 'tpope/vim-fireplace', {'for': 'clojure'}
Plug 'kien/rainbow_parentheses.vim'
Plug 'ervandew/supertab'

call plug#end()
" }}}

" Platform specific configuration {{{
if has("unix")
        " Unix/linux specific
elseif has('mac')
        " Mac specific
elseif has('win32')
        " Windows specific
endif
" }}}

" ToggleComment function declaration and configuration {{{
autocmd FileType *sh,awk,python,perl,perl6,ruby let b:cmt = exists('b:cmt') ? b:cmt : '#'
autocmd FileType c,cpp,cs,h,hpp let b:cmt = exists('b:cmt') ? b:cmt : '//'
autocmd FileType vim let b:cmt = exists('b:cmt') ? b:cmt : '"'
autocmd FileType clojure let b:cmt = exists('b:cmt') ? b:cmt : ';'
autocmd FileType tex,latex,plaintex let b:cmt = exists('b:cmt') ? b:cmt : '%'
autocmd BufNewFile,BufRead *.vim,.vimrc let b:cmd = exists('b:cmt') ? b:cmt : '"'
autocmd BufNewFile,BufRead * let b:cmd = exists('b:cmt') ? b:cmt : '#'

function! ToggleComment() range
        let comment_char = exists('b:cmt') ? b:cmt : '#'
        let linenum = a:firstline

        let currline = getline(a:firstline, a:lastline)
        if currline[0] =~ '^' . comment_char
                for line in currline
                        let repline = substitute(line, '^' . comment_char, "", "")
                        call setline(linenum, repline)
                        let linenum += 1
                endfor
        else
                for line in currline
                        let repline = substitute(line, '^\(' . comment_char .'\)\?', comment_char, "")
                        call setline(linenum, repline)
                        let linenum += 1
                endfor
        endif
endfunction
" }}}

" Rebind searching and substitution to use all special characters {{{
nnoremap / /\v
vnoremap / /\v
cnoremap %s/ %smagic/
cnoremap \>s/ \>smagic/
nnoremap :g/ :g/\v
nnoremap :g// :g//
" }}}

" Plugin and function keybindings {{{
nmap <silent> <LocalLeader>/ :call ToggleComment()<CR>j0
vmap <silent> <LocalLeader>/ :call ToggleComment()<CR>

" vim-fireplace bindings
autocmd FileType clojure nnoremap <buffer> <localleader>re :Eval<cr>
autocmd FileType clojure vnoremap <buffer> <localleader>re :Eval<cr>
autocmd FileType clojure nnoremap <buffer> <localleader>rf :%Eval<cr>
autocmd FileType clojure nnoremap <buffer> <localleader>rr :Require<cr>
autocmd FileType clojure nnoremap <buffer> <localleader>rR :Require!<cr>
autocmd FileType clojure nnoremap <buffer> <localleader>rt :RunTests<cr>
autocmd FileType clojure nnoremap <buffer> <localleader>rl :Last<cr>
autocmd FileType clojure nnoremap <buffer> <localleader>rc :FireplaceConnect<cr>
" }}}

" Plugin configuration {{{
" Enable rainbow parentheses always
au VimEnter * RainbowParenthesesToggle
au Syntax * RainbowParenthesesLoadRound
au Syntax * RainbowParenthesesLoadSquare
au Syntax * RainbowParenthesesLoadBraces
" }}}

colorscheme wombat
