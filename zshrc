#################################
##                             ##
##  General purpose functions  ##
##                             ##
#################################

## Print a message to the terminal if the shell
## is interactive
## $1 - The message to print
function terminal-log() {
	if [[ -o interactive ]]; then
		echo "ZSHRC: $1"
	fi
}

## Detect if the given directory can be found in the current $PATH
## $1 - The complete path of the directory to check for. Eg, /opt/application/bin
## Returns - true if the directory is found, otherwise false
function dir-present-in-path() {
        if [[ ":$PATH:" == *":$1:"* ]]; then
                true
        else
                false
        fi
}

## Add a directory to the front of the current $PATH if it doesn't already exist in the $PATH
## $1 - The complete path of the directory to add. Eg, /opt/application/bin
## $2 - Friendly name for the path, used in logging
function prepend-dir-to-path() {
        if ! dir-present-in-path $1; then
                PATH=$1:$PATH

                terminal-log "$2 added to PATH"
        fi
}

## Remove a directory from the $PATH if it is present in $PATH
## $1 - The complete path of the directory to add. Eg, /opt/application/bin
## $2 - Friendly name for the path, used in logging
function remove-dir-from-path() {
        if dir-present-in-path $1; then
                WORK=:$PATH:
                WORK=${WORK/:$1:/:}
                WORK=${WORK%:}
                WORK=${WORK#:}
                PATH=$WORK

                terminal-log "$2 removed from PATH"
        fi
}

## Add or remove a directory from the $PATH depending on whether it is present already
## $1 - The complete path of the directory to toggle. Eg, /opt/application/bin
## $2 - Friendly name for the path, used in logging
function toggle-dir-in-path() {
        if dir-present-in-path $1; then
                remove-dir-from-path $1 $2
        else
                prepend-dir-to-path $1 $2
        fi
}

## Create a symlink to a file or folder, optionally recreating the symlink
## $1 - The complete path of the symlink. Eg, $HOME/.dotf 
## $2 - The complete path of the destination path for symlink. Eg, $HOME/dotfiles/dotf
## $3 - OPTIONAL Remove existing file/symlinks and recreate [true|false]
function ensure-symbolic-link() {
        if [[ $# -eq 3 && $3 == true ]]; then
                if [ -e $1 ]; then
                        terminal-log "$1 deleted"
                        rm $1
                fi
        fi

        if [ ! -e $1 ]; then
                terminal-log "Symlink $1 -> $2 created"
                ln -s $2 $1
        fi
}


#################################
##                             ##
##  Platform enable functions  ##
##                             ##
#################################

## Add or remove Anaconda binaries from PATH. Allows for 
## separation of platform python and anaconda python
function toggle-anaconda() {
	if [ ! -z $ANACONDA_BASE ]; then
		if [ -d "$ANACONDA_BASE/bin" ]; then 
			toggle-dir-in-path "$ANACONDA_BASE/bin" "Anaconda"
		else
			terminal-log "ANACONDA_BASE defined, but is invalid"
		fi
	fi
}

#################################
##                             ##
##   Configuration functions   ##
##                             ##
#################################

## Determine if the platform is WSL, MAC or LINUX, exporting the result to PLATFORM_NAME.
## If WSL Is detected, windows binary directories are removed from PATH, and DISPLAY is set
function configure-platform() {
	if [[ "$(uname)" == Darwin* ]]; then
		export PLATFORM_NAME="MAC"
	elif [[ "$(uname -s)" == Linux* ]]; then
		# WSL vs Native options
		if grep -q microsoft /proc/version; then
			export PLATFORM_NAME="WSL"

			# Remove windows paths from WSL path
			PATH=$(echo $PATH | sed -e 's/:\/mnt.*//g')

			# Support displaying X11 applications on windows
			export DISPLAY="127.0.0.1:0.0"
		else
			export PLATFORM_NAME="LINUX"
		fi
	fi
}

## Neovim specific setup
function configure-neovim() {
	if [ -x "$(command -v neovim)" ]; then
		# Check for the neovim gem and if it is present add it to the PATH
		NEOVIM_GEM=$(gem which neovim | sed -e 's/lib\/neovim\.rb/bin/g')

		if [ -d "$NEOVIM_GEM" ]; then
			prepend-dir-to-path $NEOVIM_GEM "Neovim gem"
		fi
	fi
}

## If texlive is present, setup PATH, MANPATH and INFOPATH
function configure-texlive() {
        if [ ! -z "$TEXLIVE_BASE" ]; then
                # Verify the decided directory exists
                if [ -d "$TEXLIVE_BASE" ]; then
                        # Add bin dir(s) to PATH
                        for bin_dir in $TEXLIVE_BASE/bin/*; do
                                prepend-dir-to-path $bin_dir "Texlive binaries"
                        done

                        # Configure documentation
                        export MANPATH=$TEXLIVE_BASE/texmf-dist/doc/man:$MANPATH
                        export INFOPATH=$TEXLIVE_BASE/texmf-dist/doc/info:$INFOPATH
                else
                        terminal-log "TEXLIVE_BASE defined, but is invalid"
                fi
        fi
}

## Setup platform specific, and general terminal aliases
function configure-aliases() {
	# Platform specific aliases
	if [[ $PLATFORM_NAME == "MAC" ]]; then
		alias la='ls -Glah'
		alias ll='ls -Glh'
		alias ls='ls -Gl'
	elif [[ $PLATFORM_NAME == "LINUX" ]]; then
		# NOOP
	elif [[ $PLATFORM_NAME == "WSL" ]]; then
		# NOOP
	fi

	if [[ ($PLATFORM_NAME == "LINUX" || $PLATFORM_NAME == "WSL") ]]; then
		if [ -f "$HOME/.dircolors" ]; then
			source "$HOME/.dircolors"
		fi

		if (whence apt &> /dev/null); then
			alias apthup='sudo apt update &> /dev/null && sudo apt list --upgradable'
		fi

		alias la='ls -lah --color=auto'
		alias ll='ls -lh --color=auto'
		alias ls='ls -l --color=auto'
	fi

	#General aliases
	if (whence tmux &> /dev/null); then
		alias tj='tmux attach -t'
		alias tk='tmux kill-session -t'
		alias tn='tmux new -s'
	fi
}

## Load user binary directories
function configure-user-bin() {
	# Create user-bin dir and add to PATH
	mkdir -p "$HOME/bin"
	if [ -d "$HOME/bin" ]; then
		prepend-dir-to-path $HOME/bin "User bin"
	fi
}

## ZSH options
function configure-zsh() {
	# Environment variables are defined in zprofile,
	# and this is only sourced by login shells. Check
	# for situations where it won't be sourced, and 
	# manually source it as appropriate
	if [[ ! -o login ]]; then
		terminal-log "Running a non-login shell"

		if [ -z $ZPROFILE_SOURCED ]; then
			if [ -f "$HOME/.zprofile" ]; then
				terminal-log "Sourcing zprofile"
				source "$HOME/.zprofile"
			fi
		fi
	fi


	# Clone zprezto if it doesn't yet exist
	if [ ! -d "${ZDOTDIR:-$HOME}/.zprezto" ]; then
		git clone --recursive https://github.com/sorin-ionescu/prezto.git "${ZDOTDIR:-$HOME}/.zprezto"
	fi

	# Load the zprezto configuration
	source "${ZDOTDIR:-$HOME}/.zprezto/init.zsh"

	# Where to save history to disk
	HISTFILE="${ZDOTDIR:-$HOME}/.zsh_history"

	# How many lines of history to keep in memory
	HISTSIZE=5000

	# Number of history entries to save to disk
	SAVEHIST=5000

	# cd adds to the dirstack like pushd
	setopt auto_pushd

	# pushd works the same as cd with no args, move to $HOME
	setopt pushd_to_home

	# Append history to the history file (no overwriting)
	setopt append_history

	# Share history across terminals
	setopt share_history

	# Immediately append to the history file, not just when a term is killed
	setopt inc_append_history

	# When scrolling through history, only show unique options
	setopt hist_find_no_dups

	# Don't dumplicate previous command in history
	setopt hist_ignore_dups

	# Expand parameters in shell prompt
	setopt prompt_subst
}


#################################
##                             ##
##        Perform setup        ##
##                             ##
#################################

configure-zsh
configure-platform
configure-texlive
configure-neovim
configure-aliases
configure-user-bin

# Clear out the configuration functions
unfunction configure-zsh
unfunction configure-platform
unfunction configure-texlive
unfunction configure-neovim
unfunction configure-aliases
unfunction configure-user-bin

